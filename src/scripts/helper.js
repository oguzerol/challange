var charMap = {
  Ç: 'C',
  Ö: 'O',
  Ş: 'S',
  İ: 'I',
  I: 'i',
  Ü: 'U',
  Ğ: 'G',
  ç: 'c',
  ö: 'o',
  ş: 's',
  ı: 'i',
  ü: 'u',
  ğ: 'g'
};

export const searchInsensetiveText = (str, search_str) => {
  let str_array = search_str.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "").split('');
  for (var i = 0, len = str_array.length; i < len; i++) {
    str_array[i] = charMap[str_array[i]] || str_array[i];
  }
  let result = str.search(new RegExp(search_str = str_array.join('').replace(/[çöşüğı]/gi, ""), "i"));
  return (result >= 0) ? true : false;
};

export const delay = (function () {
  var timer = 0;
  return function (callback, ms) {
    clearTimeout(timer);
    timer = setTimeout(callback, ms);
  };
})();