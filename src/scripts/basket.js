
const basketList = [];

import {getMenuItemById} from './data';
import { delay } from './helper';

const categoriesListElement = document.querySelector('.categories-list');
const basketItemListElement = document.querySelector('.basket-item-list');
const getExtendedPrice = (item) => parseFloat(item.ExtendedPrice.replace(',', '.'));

const basketItem = function (id, name, price, count) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.count = count;
};

const addItemToBasket = (id, count) => {
    let exactItem = getMenuItemById(id);
    if (exactItem) {
        for (let i in basketList) {
            if (basketList[i].id === id) {
                basketList[i].count = parseFloat(count) + parseFloat(basketList[i].count);
                basketList[i].price = (getExtendedPrice(exactItem) * parseFloat(basketList[i].count)).toFixed(2);
                return;
            }
        }
        basketList.push(new basketItem(exactItem.ProductId, exactItem.DisplayName, exactItem.ExtendedPrice, count));
    }
};

const removeItemFromBasket = (id) => {
    for (let i in basketList) {
        if (basketList[i].id === id) {
            basketList.splice(i, 1);
            break;
        }
    }
};


const UpdateItemAmount = (id, newCount) => {
    let exactItem = getMenuItemById(id);
    if(newCount == 0) {
        removeItemFromBasket(id);
        return;
    }
    for (let i in basketList) {
        if (basketList[i].id === id) {
            basketList[i].count = newCount;
            basketList[i].price = (getExtendedPrice(exactItem) * parseFloat(newCount)).toFixed(2);
            break;
        }
    }
};

const calculateTotalAmount = () => {
    let totalCount = 0;
    for (let i in basketList) {
        let exactItem = getMenuItemById(basketList[i].id);
        let itemCount = (getExtendedPrice(exactItem) * parseFloat(basketList[i].count)).toFixed(2);
        totalCount += parseFloat(itemCount);
    }
    return totalCount.toFixed(2);
};










const renderBaskets = basketList => {
    let basketListHtml = '';
    for (let i = 0; i < basketList.length; i++) {
        basketListHtml += basketRenderItem(basketList[i]);
    }
    if (basketList.length) {
        basketListHtml += basketRenderTotalAmount(calculateTotalAmount());
    } else {
        basketListHtml += `<p class="basket-empty-list">Sepeteniz Boş</p>`;
    }

    basketItemListElement.innerHTML = basketListHtml;
};

const basketRenderItem = item => 
        `<div class="basket-item-wrapper ">
            <div class="basket-item-info">
                <p class="basket-item-info-title">
                    ${item.name}
                </p>
            </div>
            <div class="basket-item-count">
                <input type="number" class="basket-item-count__input"  data-id="${item.id}" value="${item.count}"> 
            </div>
            <div class="basket-item-total">
                <span class="basket-item-total-count">${item.price} TL</span>
                <button class="basket-item-delete" data-id="${item.id}"> X </button>
            </div>
        </div>`;


const basketRenderTotalAmount = (count) =>
    `<div class="basket-total-amount">
        <span>Toplam : </span>
        <span>${count} TL</span>
     </div>`;


categoriesListElement.addEventListener('click', (e) => {
    if (e.target.className === 'category-item__add__button') {
        addItemToBasket(e.target.parentElement.dataset.id, e.target.parentElement.querySelector('input').value);
        renderBaskets(basketList);
        e.target.previousElementSibling.value = 1;
    }
});
basketItemListElement.addEventListener('click', (e) => {
    if (e.target.className === 'basket-item-delete') {
        removeItemFromBasket(e.target.dataset.id);
        renderBaskets(basketList);
    }
});
basketItemListElement.addEventListener('keyup', (e) => {
    if (e.target.className === 'basket-item-count__input') {
        delay(function () {
            UpdateItemAmount(e.target.dataset.id,e.target.value);
            renderBaskets(basketList);
        }, 400);
    }
});