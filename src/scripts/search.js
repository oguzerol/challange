
import {searchInsensetiveText} from './helper';
import { delay } from './helper';
import {restaurantsMenu} from './data';
const rightElement = document.querySelector('.right');
const leftElement = document.querySelector('.left');


var searchBox = document.querySelector('.search-box');
var categoriesListElement = document.querySelector('.categories-list');

searchBox.addEventListener('keyup', function () {
    delay(function () {
        const searchBoxValue = searchBox.value.trim();
        if (searchBoxValue.length > 2) {
            categoriesListElement.innerHTML = '';
            renderProducts(searchMenuItem(restaurantsMenu, searchBoxValue));
        } else {
            if (searchBoxValue !== '' || searchBoxValue.length == 0) {
                categoriesListElement.innerHTML = ' ';
                renderProducts(restaurantsMenu);
            }
        }
    }, 250);
});

const searchMenuItem = (menu, foodName) => 
    menu.map(menuGroup => {
        let Products = menuGroup.Products.filter(menuItem => searchInsensetiveText(menuItem.DisplayName, foodName));
        if (Products.length > 0) {
            return Object.assign({}, menuGroup, { Products });
        } else {
            return undefined;
        }
    }).filter(a => a);
    

const renderProductItem = categoryItem => 
        `<div class="category-item">
            <div class="category-item__add" data-id="${categoryItem.ProductId}">
                <input type="" class="category-item__add__input" value="1">
                <button class="category-item__add__button">+</button>
            </div>
            <div class="category-item__info">
                <p class="category-item__info__name">
                    ${categoryItem.DisplayName}
                </p>
                <p class="category-item__info__detail">
                    ${categoryItem.Description}
                </p>
            </div>
            <div class="category-item__price">
                <span>${categoryItem.ExtendedPrice} TL</span>
            </div>
        </div>`;


export const renderProducts = menu => {
    let categoryItemHtml = '';

    console.log('test');
    for (let i = 0; i < menu.length; i++) {
        let itemPerGroup = '';
        for (let j = 0; j < menu[i].Products.length; j++) {
            itemPerGroup += renderProductItem(menu[i].Products[j]);
        }
        categoryItemHtml += `<div class="category-list-item-wrapper">
        <div class="category-header">
         <p>${menu[i].CategoryDisplayName}</p>
        </div>
        <div class="category-content">
            ${itemPerGroup}
        </div>
      </div>`;
    }
    categoriesListElement.innerHTML += categoryItemHtml;
};