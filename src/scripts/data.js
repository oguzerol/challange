import rawData from '../data/menuData.json';

export const restaurantsMenu = rawData.d.ResultSet;

export const getMenuItemById = id => {
    for(let i=0; i < restaurantsMenu.length; i++) {
        let Products = restaurantsMenu[i].Products.filter(menuItem => menuItem.ProductId === id);
        if (Products.length > 0) {
            return Products[0];
        } 
    }
};
